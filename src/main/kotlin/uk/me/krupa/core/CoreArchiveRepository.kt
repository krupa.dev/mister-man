package uk.me.krupa.core

import java.util.zip.ZipOutputStream

interface CoreArchiveRepository {
    fun downloadToZip(id: String, token: String, zip: ZipOutputStream, repo: String, version: String)
}
