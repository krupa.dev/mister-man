package uk.me.krupa.core

interface DownloadNotifier {
    fun notify(id: String, state: String, progress: Int, maximum: Int, done: Boolean = false)
    fun notify(id: String, source: String, destination: String)
}
