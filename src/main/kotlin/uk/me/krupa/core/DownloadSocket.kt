package uk.me.krupa.core

import com.fasterxml.jackson.databind.ObjectMapper
import mu.KotlinLogging
import uk.me.krupa.auth.IdentityService
import java.util.concurrent.ConcurrentHashMap
import javax.enterprise.context.ApplicationScoped
import javax.websocket.*
import javax.websocket.server.PathParam
import javax.websocket.server.ServerEndpoint


@ServerEndpoint("/ws/download/{id}")
@ApplicationScoped
class DownloadSocket(
    private val identityService: IdentityService,
    private val objectMapper: ObjectMapper,
    private val downloadRepository: DownloadRepository
): DownloadNotifier {

    private val logger = KotlinLogging.logger {  }

    val sessions = ConcurrentHashMap<String, Session>()
    val states = ConcurrentHashMap<String,SocketState>()

    @OnOpen
    fun onOpen(session: Session, @PathParam("id") id: String) {
        sessions[id] = session
        logger.info { "ID $id joined" }
        session.asyncRemote.sendText("authenticate")
        states[id] = SocketState.AUTHENTICATING
    }

    @OnClose
    fun onClose(session: Session?, @PathParam("id") id: String) {
        sessions.remove(id)
        logger.info { "ID $id left" }
    }

    @OnError
    fun onError(session: Session?, @PathParam("id") id: String, throwable: Throwable) {
        sessions.remove(id)
        logger.info { "ID $id left on error $throwable" }
    }

    @OnMessage
    fun onMessage(message: String, @PathParam("id") id: String) {
        if (states[id] == SocketState.AUTHENTICATING) {
            if (identityService.userIdentity(message).isAnonymous) {
                sessions[id]?.close(CloseReason(CloseReason.CloseCodes.VIOLATED_POLICY, "Authentication failed"))
            } else {
                states[id] = SocketState.AUTHENTICATED
                sessions[id]?.asyncRemote?.sendText("OK")
                downloadRepository.startDownload(id, message)
            }
        } else {
            logger.info { "ID $id send a message $message" }
        }
    }

    override fun notify(id: String, state: String, progress: Int, maximum: Int, done: Boolean) {
        sessions[id]?.asyncRemote?.sendText(objectMapper.writeValueAsString(
            DownloadProgress(state, progress, maximum, done)
        ))
    }

    override fun notify(id: String, source: String, destination: String) {
        sessions[id]?.asyncRemote?.sendText(objectMapper.writeValueAsString(
            DownloadFile(source, destination)
        ))
    }
}
