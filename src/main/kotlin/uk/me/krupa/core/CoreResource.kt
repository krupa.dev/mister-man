package uk.me.krupa.core

import io.quarkus.security.Authenticated
import io.quarkus.security.identity.SecurityIdentity
import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.Uni
import uk.me.krupa.github.GitHubRefresh
import java.io.File
import java.util.*
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/api/core")
class CoreResource(
    @Inject private val coreRepository: CoreRepository,
    @Inject private val securityIdentity: SecurityIdentity,
    @Inject private val downloadRepository: DownloadRepository,
    @Inject private val gitHubRefresh: GitHubRefresh
) {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun selectedCores(): Multi<CoreIdentity> =
        coreRepository.selectedCores(securityIdentity.principal.name)

    @GET
    @Path("/available")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun availableCores(): Multi<String> =
        coreRepository.availableCores(securityIdentity.principal.name)

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    fun addCore(coreIdentity: CoreIdentity) =
        coreRepository.addCore(securityIdentity.principal.name, coreIdentity)

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun version(@PathParam("id") id: String): Multi<String> =
        coreRepository.getVersions(securityIdentity.principal.name, id)

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Authenticated
    fun updateCore(@PathParam("id") id: String, value: CoreSelection): Uni<Unit> =
        coreRepository.updateVersion(securityIdentity.principal.name, id, value.version!!)

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun deleteCore(@PathParam("id") id: String): Uni<Unit> =
        coreRepository.removeCore(securityIdentity.principal.name, id)

    @POST
    @Path("/download")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun requestDownload(): Uni<DownloadIdentity> =
        downloadRepository.createFile(securityIdentity.principal.name)

    @DELETE
    @Path("/download/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun deleteFile(@PathParam("id") id: String): Uni<Unit> =
        downloadRepository.deleteFile(securityIdentity.principal.name, id)

    @GET
    @Path("/download/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @Authenticated
    fun download(@PathParam("id") id: String): Uni<Response> {
        return downloadRepository.getFile(securityIdentity.principal.name, id)
            .map { File(it) }
            .map { Response.ok(it) }
            .map { it.header("Content-Disposition", "attachment;filename=mister.zip") }
            .map { it.build() }
    }

    @POST
    @Path("/refresh")
    @Produces(MediaType.APPLICATION_JSON)
    @Authenticated
    fun refresh(): Uni<Response> {
        gitHubRefresh.refreshCores()
        return Uni.createFrom().item(Response.ok().build())
    }
}
