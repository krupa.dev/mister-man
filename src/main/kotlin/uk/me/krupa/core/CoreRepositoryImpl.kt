package uk.me.krupa.core

import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.pgclient.PgPool
import io.vertx.mutiny.sqlclient.Tuple
import mu.KotlinLogging
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class CoreRepositoryImpl(
    @Inject private val db: PgPool
): CoreRepository {

    private val logger = KotlinLogging.logger {  }

    override fun availableCores(username: String): Multi<String> =
        db.preparedQuery(
            """
            SELECT DISTINCT
                v.repo AS repo
            FROM version v
            WHERE v.repo NOT IN (SELECT repo FROM user_selection WHERE username=$1) 
            ORDER BY repo ASC
            """.trimIndent()).execute(Tuple.of(username))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { it.getString("repo") }

    override fun selectedCores(username: String): Multi<CoreIdentity> =
        db.preparedQuery(
            """
            SELECT
                repo,
                name,
                selected_version AS version
            FROM user_selection
            WHERE username = $1
            ORDER BY CASE WHEN name IS NOT NULL THEN name ELSE repo END
            """.trimIndent()).execute(Tuple.of(username))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform {
                CoreSelection(
                    it.getString("name") ?: "",
                    it.getString("repo"),
                    it.getString("version")
                )
            }

    override fun addCore(username: String, coreIdentity: CoreIdentity): Uni<Unit> {
        return db.preparedQuery(
            """
            INSERT INTO user_selection (username, repo, name, selected_version) VALUES(
                $1,
                $2,
                $3,
                NULL
            )
            """.trimIndent())
            .execute(Tuple.of(username, coreIdentity.repo, coreIdentity.name)).map { Unit }
    }

    override fun removeCore(username: String, repo: String): Uni<Unit> {
        logger.info { "Delete $repo from $username" }
        return db.preparedQuery(
            """
            DELETE FROM user_selection WHERE
                username=$1 AND
                repo=$2
            """.trimIndent())
            .execute(Tuple.of(username, repo)).map { Unit }
    }

    override fun getVersions(username: String, repo: String): Multi<String> =
        db.preparedQuery(
            """
            SELECT DISTINCT
                version
            FROM version
            WHERE repo=$1 
            ORDER BY version DESC
            """.trimIndent()).execute(Tuple.of(repo))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { it.getString("version") }

    override fun updateVersion(username: String, repo: String, version: String): Uni<Unit> =
        db.preparedQuery(
            """
            UPDATE user_selection SET selected_version=$3 WHERE
                username=$1 AND
                repo=$2
            """.trimIndent())
            .execute(Tuple.of(username, repo, version)).map { Unit }

}
