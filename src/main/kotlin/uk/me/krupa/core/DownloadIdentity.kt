package uk.me.krupa.core

import com.fasterxml.jackson.annotation.JsonProperty

open class DownloadIdentity(
    @field:JsonProperty("id")
    val id: String?
)
