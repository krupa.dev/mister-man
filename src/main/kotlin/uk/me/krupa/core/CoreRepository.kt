package uk.me.krupa.core

import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.Uni

interface CoreRepository {
    fun availableCores(username: String): Multi<String>
    fun selectedCores(username: String): Multi<CoreIdentity>
    fun addCore(username: String, newData: CoreIdentity): Uni<Unit>
    fun removeCore(username: String, repo: String): Uni<Unit>
    fun getVersions(username: String, repo: String): Multi<String>
    fun updateVersion(username: String, repo: String, version: String): Uni<Unit>
}
