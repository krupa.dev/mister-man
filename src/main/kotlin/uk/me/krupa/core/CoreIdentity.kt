package uk.me.krupa.core

import com.fasterxml.jackson.annotation.JsonProperty

open class CoreIdentity(
    @field:JsonProperty("name")
    val name: String? = null,

    @field:JsonProperty("repo")
    val repo: String? = null
)
