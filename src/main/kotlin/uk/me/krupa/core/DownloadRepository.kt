package uk.me.krupa.core

import io.smallrye.mutiny.Uni

interface DownloadRepository {
    fun createFile(username: String): Uni<DownloadIdentity>
    fun startDownload(id: String, token: String)
    fun deleteFile(username: String, id: String): Uni<Unit>
    fun getFile(username: String, id: String): Uni<String>
}
