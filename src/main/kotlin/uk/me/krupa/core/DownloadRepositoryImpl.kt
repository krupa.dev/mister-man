package uk.me.krupa.core

import io.quarkus.scheduler.Scheduled
import io.smallrye.mutiny.Multi
import io.smallrye.mutiny.Uni
import io.vertx.mutiny.pgclient.PgPool
import io.vertx.mutiny.sqlclient.Tuple
import mu.KotlinLogging
import org.eclipse.microprofile.context.ManagedExecutor
import uk.me.krupa.auth.IdentityService
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files
import java.util.*
import java.util.zip.ZipOutputStream
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import kotlin.io.path.absolutePathString

@ApplicationScoped
class DownloadRepositoryImpl(
    @Inject private val db: PgPool,
    @Inject private val downloadNotifier: DownloadNotifier,
    @Inject private val managedExecutor: ManagedExecutor,
    @Inject private val coreArchiveRepository: CoreArchiveRepository,
    @Inject private val identityService: IdentityService
): DownloadRepository {

    private val logger = KotlinLogging.logger {  }

    override fun createFile(username: String): Uni<DownloadIdentity> {
        val id = UUID.randomUUID()

        return db.preparedQuery("""
            DELETE FROM download
            WHERE username=$1
            RETURNING filename
        """.trimIndent())
            .execute(Tuple.of(username))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform {
                val filename = it.getString("filename")
                logger.info { "Deleting old file $filename" }
                File(filename).delete()
            }.collect().last().chain { ->
                val destination = Files.createTempFile("misterMan", ".zip").absolutePathString()
                db.preparedQuery("""
                    INSERT INTO download (id, username, start_time, filename)
                    VALUES($1,$2,NOW(),$3)
                """.trimIndent()
                ).execute(Tuple.of(id, username, destination))
            }.map { DownloadIdentity(id.toString()) }
    }

    override fun startDownload(id: String, token: String) {
        managedExecutor.runAsync { doDownload(id, token) }.exceptionally { ex: Throwable ->
            logger.error(ex) { "Error running download" }
            null
        }
    }

    override fun getFile(username: String, id: String): Uni<String> {
        return db.preparedQuery("""
            SELECT filename FROM download
            WHERE id=$1 AND username=$2
        """.trimIndent())
            .execute(Tuple.of(id, username))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform {
                it.getString("filename")
            }
            .collect().first()
    }

    @Scheduled(every = "1M")
    fun cleanupDownloads() {
        logger.info { "Cleaning downloads on schedule" }
        db.query("""
            DELETE FROM download
            WHERE start_time < NOW() - interval '10 minutes'
            RETURNING filename
        """.trimIndent())
            .execute()
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform {
                val filename = it.getString("filename")
                logger.info { "Deleting old file $filename" }
                File(filename).delete()
            }
            .collect().last().await().indefinitely()
    }


    override fun deleteFile(username: String, id: String): Uni<Unit> {
        return db.preparedQuery("""
            DELETE FROM download
            WHERE id=$1 AND username=$2
            RETURNING filename
        """.trimIndent())
            .execute(Tuple.of(id, username))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform {
                File(it.getString("filename")).delete()
            }
            .onItem().transform {  }
            .collect().first()
    }

    private fun doDownload(id: String, token: String) {
        val identity = identityService.userIdentity(token)
        val filename = db.preparedQuery("""
            SELECT filename FROM download
            WHERE id=$1 AND username=$2
        """.trimIndent())
            .execute(Tuple.of(id, identity.principal.name))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { row ->
                row.getString("filename")
            }.collect().first().await().indefinitely()

        val count = db.preparedQuery("""
                SELECT
                    COUNT(*) c
                FROM
                    download dl
                    INNER JOIN user_selection us on dl.username = us.username
                WHERE
                    dl.id=$1
            """.trimIndent())
            .execute(Tuple.of(id))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { it.getInteger("c") }
            .collect().first().await().indefinitely()

        ZipOutputStream(FileOutputStream(filename)).use { zip ->
            db.preparedQuery("""
                SELECT
                    us.repo r,
                    us.selected_version v,
                    ROW_NUMBER() OVER (ORDER BY us.repo) rn
                FROM
                    download dl
                    INNER JOIN user_selection us on dl.username = us.username
                    WHERE
                    dl.id=$1 AND
                    us.selected_version IS NOT NULL
                ORDER BY
                    r
            """.trimIndent())
                .execute(Tuple.of(id))
                .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
                .onItem().transform {
                    val repo = it.getString("r")
                    val version = it.getString("v")
                    coreArchiveRepository.downloadToZip(id, token, zip, repo, version)
                    downloadNotifier.notify(id, "Downloading", it.getInteger("rn"), count)
                }
                .collect().last().await().indefinitely()
        }

        logger.info { "Done downloading ${filename}" }
        downloadNotifier.notify(id, "Done", count, count, true)
    }
}
