package uk.me.krupa.core

enum class SocketState {
    AUTHENTICATING,
    AUTHENTICATED
}
