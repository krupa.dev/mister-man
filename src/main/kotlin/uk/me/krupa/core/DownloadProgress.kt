package uk.me.krupa.core

import com.fasterxml.jackson.annotation.JsonProperty

open class DownloadProgress(
    @field:JsonProperty("state")
    val state: String,
    @field:JsonProperty("progress")
    val progress: Int,
    @field:JsonProperty("maximum")
    val maximum: Int,
    @field:JsonProperty("done")
    val done: Boolean = false
)
