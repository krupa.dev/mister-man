package uk.me.krupa.core

import com.fasterxml.jackson.annotation.JsonProperty

class CoreSelection(
    name: String? = null,
    repo: String? = null,
    @field:JsonProperty("version")
    val version: String? = null,
): CoreIdentity(name, repo)
