package uk.me.krupa.core

import com.fasterxml.jackson.annotation.JsonProperty

open class DownloadFile(
    @field:JsonProperty("source")
    val source: String,
    @field:JsonProperty("destination")
    val destination: String,
)
