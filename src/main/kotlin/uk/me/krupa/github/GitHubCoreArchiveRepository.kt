package uk.me.krupa.github

import com.gitlab.mvysny.konsumexml.anyName
import com.gitlab.mvysny.konsumexml.konsumeXml
import mu.KotlinLogging
import org.kohsuke.github.GHTreeEntry
import org.kohsuke.github.GitHub
import uk.me.krupa.core.CoreArchiveRepository
import uk.me.krupa.core.DownloadNotifier
import java.io.BufferedReader
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class GitHubCoreArchiveRepository(
    @Inject private val downloadNotifier: DownloadNotifier
): CoreArchiveRepository {

    private val logger = KotlinLogging.logger {  }

    override fun downloadToZip(id: String, token: String, zip: ZipOutputStream, repo: String, version: String) {

        logger.info { "Downloading ${repo}/${version}" }

        val git = GitHub.connectUsingOAuth(token)
        val repo = git.getRepository("MiSTer-devel/${repo}")
        val tree = repo.getCommit(repo.defaultBranch).tree.getEntry("releases").asTree()
        val prefix = version.substringBefore('_')
        val file = tree.getEntry("${version}.rbf")
        val arcadeFiles = tree.tree.filter { it.path.endsWith(".mra") }

        val rbfName = if (arcadeFiles.isEmpty()) {
            "${prefix}.rbf"
        } else {
            val rbfFiles = mutableSetOf<String>()
            BufferedReader(arcadeFiles[0].readAsBlob().reader()).use {
                it.readText().konsumeXml()
                    .child("misterromdescription") {
                        children(anyName) {
                            when(localName) {
                                "rbf" -> rbfFiles.add(text())
                                else -> skipContents()
                            }
                        }
                    }
            }
            "_Arcade/cores/${rbfFiles.first()}.rbf"
        }

        copyToStream(id, zip, repo.name, file, rbfName)

        arcadeFiles.forEach { file ->
            val output = "_Arcade/${file.path.substringAfterLast('/')}"
            copyToStream(id, zip, repo.name, file, output)
        }
    }

    private fun copyToStream(id: String, zip: ZipOutputStream, repo: String, file: GHTreeEntry, destination: String) {
        zip.putNextEntry(ZipEntry(destination))
        downloadNotifier.notify(id, "${repo}/${file.path}", destination)
        file.readAsBlob().use {
            logger.info { "Copying ${file.path} to $destination" }
            it.copyTo(zip)
        }
    }
}
