package uk.me.krupa.github

import io.quarkus.runtime.Startup
import io.quarkus.runtime.StartupEvent
import io.quarkus.scheduler.Scheduled
import io.smallrye.mutiny.Multi
import io.vertx.mutiny.pgclient.PgPool
import io.vertx.mutiny.sqlclient.Tuple
import mu.KotlinLogging
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.kohsuke.github.GitHub
import java.io.FileNotFoundException
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.Observes
import javax.inject.Inject

@Startup
@ApplicationScoped
class GitHubRefresh(
    @ConfigProperty(name = "github.client-id") val clientId: String,
    @ConfigProperty(name = "github.client-secret") val clientSecret: String,
    @Inject private val db: PgPool
) {

    companion object {
        val supportedLanguages = setOf(
            "VHDL",
            "Verilog",
            "SystemVerilog",
        )
    }

    private val logger = KotlinLogging.logger {  }

    fun allRepos(client: GitHub): List<String> {
        return client.getOrganization("MiSTer-devel")
            .listRepositories()
            .filter { it.language in supportedLanguages }
            .map { it.name }
            .sorted()
    }

    fun refreshVersions(repo: String, client: GitHub) {
        logger.info { "Examining versions for $repo" }

        val coreRepo = client.getRepository("MiSTer-devel/${repo}")
        val availableVersions = try {
            coreRepo
                .getDirectoryContent("releases", coreRepo.defaultBranch)
                .filter { it.name.endsWith(".rbf") }
                .map { it.name.removeSuffix(".rbf") }
                .toSet()
        } catch (ex: FileNotFoundException) {
            logger.info { "Core $repo does not have a releases directory" }
            setOf()
        }

        logger.info { "$repo as ${availableVersions.size} versions" }

        val knownVersions = db.preparedQuery("SELECT version FROM version WHERE repo=$1")
            .execute(Tuple.of(repo))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { it.getString("version") }
            .collect().asList().await().indefinitely().toSet()

        val toRemove = knownVersions.subtract(availableVersions)
        val toAdd = availableVersions.subtract(knownVersions)

        logger.info { "Adding ${toAdd.size} versions to $repo" }
        logger.info { "Removing ${toRemove.size} versions to $repo" }

        if (toRemove.isNotEmpty()) {
            var count = db.preparedQuery("""
                UPDATE user_selection SET selected_version=NULL WHERE
                selected_version=$1 AND
                repo=$2
            """.trimIndent())
                .executeBatch(toRemove.map { Tuple.of(it, repo) })
                .await().indefinitely().rowCount()
            logger.info { "UPDATE $count" }

            count = db.preparedQuery("""
                DELETE FROM version WHERE
                version.version=$1 AND
                repo=$2
            """.trimIndent())
                .executeBatch(toRemove.map { Tuple.of(it, repo) })
                .await().indefinitely().rowCount()
            logger.info { "DELETE $count" }
        }

        if (toAdd.isNotEmpty()) {
            val count = db.preparedQuery("""
                INSERT INTO version (repo, version) VALUES($1,$2)
            """.trimIndent())
                .executeBatch(toAdd.map { Tuple.of(repo, it) })
                .await().indefinitely().rowCount()
            logger.info { "INSERT $count" }
        }
    }

    fun refreshCores() {
        val client = GitHub.connectUsingPassword(clientId, clientSecret)

        val knownRepos = db.query("SELECT DISTINCT repo FROM version ORDER BY repo").execute()
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { it.getString("repo") }
            .collect().asList().await().indefinitely().toSet()

        val allRepos = allRepos(client).toSet()

        val toRemove = knownRepos.subtract(allRepos)
        if (toRemove.isNotEmpty()) {
            val removed = db.preparedQuery("DELETE FROM user_selection WHERE repo=$1")
                .executeBatch(toRemove.map { Tuple.of(it) })
                .await().indefinitely().rowCount()
            logger.info { "DELETE_USER_SELECTION $removed" }

            val removed1 = db.preparedQuery("DELETE FROM version WHERE repo=$1")
                .executeBatch(toRemove.map { Tuple.of(it) })
                .await().indefinitely().rowCount()
            logger.info { "DELETE_VERSION $removed1" }
        }

        logger.info { "Got ${knownRepos.size} known repos and ${allRepos.size} available repos" }

        allRepos.stream()
            .forEach {
                refreshVersions(it, client)
            }
    }

    @Scheduled(cron = "{refresh.cron}")
    fun refreshData() {
        logger.info { "Refreshing cores on schedule" }
        refreshCores()
    }

    fun startup(@Observes startupEvent: StartupEvent) {
        if (!initialRefreshPerformed()) {
            logger.info { "Refreshing cores at startup" }
            refreshCores()
        } else {
            logger.info { "Already initialised" }
        }
    }

    private fun initialRefreshPerformed(): Boolean {
        return db.query("SELECT COUNT(*) FROM version")
            .execute()
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform { it.getInteger(0) }
            .collect().first().await().indefinitely() > 0
    }

}
