package uk.me.krupa.github

import io.quarkus.cache.CacheResult
import io.quarkus.security.credential.TokenCredential
import io.quarkus.security.identity.SecurityIdentity
import io.quarkus.security.runtime.QuarkusSecurityIdentity
import io.smallrye.mutiny.Multi
import io.vertx.mutiny.pgclient.PgPool
import io.vertx.mutiny.sqlclient.Tuple
import org.apache.http.NameValuePair
import org.apache.http.client.utils.URLEncodedUtils
import org.apache.http.message.BasicNameValuePair
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.jboss.resteasy.plugins.server.embedded.SimplePrincipal
import org.kohsuke.github.GitHub
import uk.me.krupa.auth.IdentityService
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

fun List<Pair<String,String>>.toNameValuePairs() =
    map { BasicNameValuePair(it.first, it.second) }

@ApplicationScoped
class GitHubIdentityService(
    @ConfigProperty(name = "github.client-id") val clientId: String,
    @ConfigProperty(name = "github.client-secret") val clientSecret: String,
    @Inject private val db: PgPool
): IdentityService {

    @CacheResult(
        cacheName = "github-user-identity",
        lockTimeout = 30000
    )
    override fun userIdentity(token: String): SecurityIdentity {
        val conn = GitHub.connectUsingOAuth(token)
        val user = conn.myself

        val matchingUsers = db.preparedQuery("SELECT username, name, admin from \"user\" WHERE username=$1 LIMIT 1")
            .execute(Tuple.of(user.login))
            .onItem().transformToMulti { set -> Multi.createFrom().iterable(set) }
            .onItem().transform {
                QuarkusSecurityIdentity.builder()
                    .addCredential(TokenCredential(token, "token"))
                    .setPrincipal(SimplePrincipal(user.login))
                    .addAttribute("name", it.getString("name"))
                    .addRole(
                        if (it.getBoolean("admin")) {
                            "admin"
                        } else {
                            "user"
                        }
                    )
                    .build()
            }
            .collect().asList().await().indefinitely()

        return if (matchingUsers.isEmpty()) {
            db.preparedQuery("INSERT INTO \"user\" (username, name, admin) VALUES($1,$2,FALSE)")
                .execute(Tuple.of(user.login, user.name))
                .await().indefinitely()

            QuarkusSecurityIdentity.builder()
                .addCredential(TokenCredential(token, "token"))
                .setPrincipal(SimplePrincipal(user.login))
                .addAttribute("name", user.name)
                .build()
        } else {
            matchingUsers[0]
        }
    }

    fun ofFormData(data: List<Pair<String, String>>): HttpRequest.BodyPublisher =
        HttpRequest.BodyPublishers.ofString(
            URLEncodedUtils.format(data.toNameValuePairs(), Charsets.UTF_8))

    override fun codeToToken(code: String): String {
        val client = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_1_1)
            .followRedirects(HttpClient.Redirect.NORMAL)
            .connectTimeout(Duration.ofSeconds(10))
            .build()

        val data = listOf(
            "client_id" to clientId,
            "client_secret" to clientSecret,
            "code" to code
        )

        val downstreamRequest = HttpRequest.newBuilder()
            .uri(URI.create("https://github.com/login/oauth/access_token"))
            .POST(ofFormData(data))
            .header("Content-Type", "application/x-www-form-urlencoded")
            .header("Accept", "application/json")
            .build()

        val response = client.send(downstreamRequest, HttpResponse.BodyHandlers.ofString())
        return response.body()
    }
}
