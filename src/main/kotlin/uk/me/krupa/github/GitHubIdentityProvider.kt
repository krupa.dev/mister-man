package uk.me.krupa.github

import io.quarkus.security.identity.AuthenticationRequestContext
import io.quarkus.security.identity.IdentityProvider
import io.quarkus.security.identity.SecurityIdentity
import io.quarkus.security.identity.request.TokenAuthenticationRequest
import io.quarkus.security.runtime.QuarkusSecurityIdentity
import io.smallrye.mutiny.Uni
import uk.me.krupa.auth.IdentityService
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

@ApplicationScoped
class GitHubIdentityProvider(
    @Inject private val identityService: IdentityService,
) : IdentityProvider<TokenAuthenticationRequest> {
    override fun getRequestType(): Class<TokenAuthenticationRequest> {
        return TokenAuthenticationRequest::class.java
    }

    override fun authenticate(
        request: TokenAuthenticationRequest?,
        context: AuthenticationRequestContext?
    ): Uni<SecurityIdentity> = context!!.runBlocking {
        request?.token?.let {
            identityService.userIdentity(request.token.token)
        } ?: QuarkusSecurityIdentity.builder().setAnonymous(true).build()
    }
}
