package uk.me.krupa.github

import io.quarkus.security.credential.TokenCredential
import io.quarkus.security.identity.IdentityProviderManager
import io.quarkus.security.identity.SecurityIdentity
import io.quarkus.security.identity.request.AuthenticationRequest
import io.quarkus.security.identity.request.TokenAuthenticationRequest
import io.quarkus.security.runtime.QuarkusSecurityIdentity
import io.quarkus.vertx.http.runtime.security.ChallengeData
import io.quarkus.vertx.http.runtime.security.HttpAuthenticationMechanism
import io.quarkus.vertx.http.runtime.security.HttpCredentialTransport
import io.smallrye.mutiny.Uni
import io.vertx.ext.web.RoutingContext
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class GitHubAuthenticationMechanism: HttpAuthenticationMechanism {

    override fun authenticate(
        context: RoutingContext?,
        identityProviderManager: IdentityProviderManager?
    ): Uni<SecurityIdentity> {
        val header = context?.request()?.getHeader("Authorization")
        return if (header != null) {
            val items = header.split(Regex("\\s+"))
            val request = TokenAuthenticationRequest(TokenCredential(items[1], items[0]))
            identityProviderManager?.authenticate(request) ?:
                Uni.createFrom().item(QuarkusSecurityIdentity.builder().setAnonymous(true).build())
        } else {
            Uni.createFrom().item(QuarkusSecurityIdentity.builder().setAnonymous(true).build())
        }
    }

    override fun getChallenge(context: RoutingContext?): Uni<ChallengeData>? {
        return Uni.createFrom().nullItem()
    }

    override fun getCredentialTypes(): MutableSet<Class<out AuthenticationRequest>> {
        return mutableSetOf(TokenAuthenticationRequest::class.java)
    }

    override fun getCredentialTransport(): HttpCredentialTransport {
        return HttpCredentialTransport(
            HttpCredentialTransport.Type.AUTHORIZATION,
            "token"
        )
    }
}
