package uk.me.krupa.auth

import org.eclipse.microprofile.config.inject.ConfigProperty
import java.util.*
import javax.annotation.security.PermitAll
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType

@Path("/api/auth")
class AuthResource(
    @ConfigProperty(name = "github.client-id") val clientId: String,
    @Inject private val identityService: IdentityService
) {
    @GET
    @Path("/data")
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    fun authClientData() = AuthData(
        clientId,
        UUID.randomUUID().toString()
    )

    @POST
    @Path("/token")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PermitAll
    fun token(request: TokenRequest): String =
        if (request.code != null) {
            identityService.codeToToken(request.code)
        } else {
            throw IllegalArgumentException("No code provided")
        }
}
