package uk.me.krupa.auth

import com.fasterxml.jackson.annotation.JsonProperty

data class AuthData(
    @field:JsonProperty("clientId")
    val clientId: String,

    @field:JsonProperty("state")
    val state: String
)
