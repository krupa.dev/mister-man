package uk.me.krupa.auth

import io.quarkus.security.identity.SecurityIdentity

interface IdentityService {
    fun userIdentity(token: String): SecurityIdentity
    fun codeToToken(code: String): String
}
