package uk.me.krupa.auth

import com.fasterxml.jackson.annotation.JsonProperty

data class TokenRequest(
    @field:JsonProperty("code")
    val code: String? = null
)
