import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import reportWebVitals from './reportWebVitals';
import AuthenticatedApp from "./auth/AuthenticatedApp";
import App from "./App";

ReactDOM.render(
    <AuthenticatedApp>
        <App/>
    </AuthenticatedApp>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
