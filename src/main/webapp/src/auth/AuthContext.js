import React from 'react'

export const AuthContext = React.createContext({
    token: '',
    error: '',
    login: () => {},
    logout: () => {}
})
