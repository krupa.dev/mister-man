import React, {Component} from "react";
import {AuthContext} from "./AuthContext";

class ShowWhenAuthenticated extends Component {
    render() {
        return (
            <AuthContext.Consumer>
                {({token}) => {
                    if (token) {
                        return this.props.children
                    }
                }}
            </AuthContext.Consumer>
        )
    }

}
export default ShowWhenAuthenticated
