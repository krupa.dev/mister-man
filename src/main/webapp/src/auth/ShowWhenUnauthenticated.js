import React, {Component} from "react";
import {AuthContext} from "./AuthContext";
import qs from "qs";

class ShowWhenAuthenticated extends Component {
    render() {
        return (
            <AuthContext.Consumer>
                {({token}) => {
                    const opts = qs.parse(window.location.search, { ignoreQueryPrefix: true })
                    if (!token && !opts.code) {
                        return this.props.children
                    }
                }}
            </AuthContext.Consumer>
        )
    }
}
export default ShowWhenAuthenticated
