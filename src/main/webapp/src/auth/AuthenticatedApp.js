import React, {Component} from "react";
import {AuthContext} from "./AuthContext";
import qs from "qs";
import axios from "axios";

class AuthenticatedApp extends Component {

    constructor(props) {
        super(props);

        this.setToken = (value) => {
            this.setState(state => ({
                token: value
            }))
        }

        this.login = () => {
            axios.get("/api/auth/data")
                .then(res => {
                    if (res.status < 200 || res.status > 299) {
                        throw new Error(`Got error code ${res.status}`)
                    } else return res
                })
                .then((res) => {
                    const data = res.data
                    const clientId = encodeURIComponent(data.clientId)
                    const state = encodeURIComponent(data.state)
                    const here = encodeURIComponent(window.location.href)
                    const to = `https://github.com/login/oauth/authorize?client_id=${clientId}&redirect_uri=${here}&scope=user&state=${state}`
                    console.log(`Redirect to ${to}`)
                    window.location.href = to
                })
                .catch((error) => {
                    console.log(error)
                    this.setState({
                        error: error.message
                    })
                })
        }

        this.logout = () => {
            this.setState({
                token: '',
                error: ''
            })
        }

        this.state = {
            token: '',
            error: '',
            login: this.login,
            logout: this.logout
        }
    }

    convertToken(code) {
        axios.post('/api/auth/token', {code: code})
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res
            })
            .then((res) => {
                axios.defaults.headers.common['Authorization'] = `token ${res.data.access_token}`
                this.setState({
                    token: res.data.access_token
                })
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
    }

    componentDidMount() {
        const {token} = this.context
        if (!token) {
            const url = window.location.href;
            const opts = qs.parse(window.location.search, { ignoreQueryPrefix: true })

            // If Github API returns the code parameter
            if (opts.code) {
                const newUrl = url.split("?");
                window.history.pushState({}, null, newUrl[0]);
                this.convertToken(opts.code)
            }
        }
    }

    render() {
        return (
            <AuthContext.Provider value={this.state}>
                {this.props.children}
            </AuthContext.Provider>
        )
    }
}

AuthenticatedApp.contextType = AuthContext

export default AuthenticatedApp
