import './App.css';
import {Button, Col, Container, Nav, Navbar, Row} from "react-bootstrap";
import Cores from "./cores/Cores";
import {HashRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import AddCore from "./cores/AddCore";
import {Component} from "react";
import {AuthContext} from "./auth/AuthContext";
import {LinkContainer} from 'react-router-bootstrap'
import ShowWhenAuthenticated from "./auth/ShowWhenAuthenticated";
import ShowWhenUnauthenticated from "./auth/ShowWhenUnauthenticated";
import {AiFillGithub, FiLogOut} from "react-icons/all";
import Download from "./cores/Download";

class App extends Component {

    render() {
        return (
            <Router>
                <Navbar bg="dark" variant="dark">
                    <Container className="container-fluid">
                        <Nav.Item>
                            &nbsp;
                            <img
                                alt=""
                                src="/logo192.png"
                                width="30"
                                height="30"
                                className="d-inline-block align-top"
                            />
                            &nbsp;
                            <LinkContainer to="/">
                                <Navbar.Brand>MiSTer-Man(ager)</Navbar.Brand>
                            </LinkContainer>
                        </Nav.Item>
                        <Nav>
                            <Nav.Item className="ml-auto">
                                <ShowWhenAuthenticated>
                                    <AuthContext.Consumer>
                                        {({token, error, login, logout}) =>  (
                                            <Nav.Link onClick={() => logout()}>
                                                Logout <FiLogOut/>
                                            </Nav.Link>
                                        )}
                                    </AuthContext.Consumer>
                                </ShowWhenAuthenticated>
                            </Nav.Item>
                        </Nav>
                    </Container>
                </Navbar>
                <br />

                <ShowWhenUnauthenticated>
                    <AuthContext.Consumer>
                        {({token, error, login, logout}) => {
                            return (
                                <Container>
                                    <Row>
                                        <Col>
                                            <Button onClick={login}>
                                                <AiFillGithub/> Login with GitHub
                                            </Button>
                                        </Col>
                                    </Row>
                                </Container>
                            )
                        }}
                    </AuthContext.Consumer>
                </ShowWhenUnauthenticated>


                <ShowWhenAuthenticated>
                    <Switch>
                        <Route path="/cores/add">
                            <AddCore/>
                        </Route>
                        <Route path="/cores/download">
                            <Download/>
                        </Route>
                        <Route path="/cores">
                            <Cores/>
                        </Route>
                        <Route path="/">
                            <Redirect to="/cores"/>
                        </Route>
                    </Switch>
                </ShowWhenAuthenticated>
            </Router>
        );
    }
}

export default App;
