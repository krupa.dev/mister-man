import React, {Component} from 'react'
import {Alert, Button, Col, Container, ProgressBar, Row} from "react-bootstrap";
import {AuthContext} from "../auth/AuthContext";
import {AiOutlineCloudDownload, ImCancelCircle} from "react-icons/all";
import axios from "axios";
import {withRouter} from "react-router-dom";
import fileDownload from 'js-file-download'

class Download extends Component {

    state = {
        id: null,
        error: null,
        socket: null,
        closeIsError: false,
        status: 'Preparing...',
        done: false,
        progress: 0,
        files: []
    }

    onMessage(event) {
        if (event.data === 'authenticate') {
            const {token} = this.context
            this.state.socket.send(token)
        } else if (event.data === 'OK') {
            // No-op
        } else {
            const obj = JSON.parse(event.data)
            console.log(obj)
            const update = {}
            if (obj.state) {
                update.status = obj.state
            }
            if (obj.progress !== undefined && obj.maximum !== undefined) {
                update.progress = (obj.progress / obj.maximum) * 100
            }
            if (obj.source !== undefined && obj.destination !== undefined) {
                update.files = this.state.files.concat(obj)
            }

            this.setState(update)

            if (obj.done) {
                this.setState({
                    closeIsError: false
                })
                this.state.socket.close()
                this.setState({
                    done: true,
                    socket: null,
                    closeIsError: false
                })
            }
        }
    }

    connectSocket(id) {
        const url = new URL(`/ws/download/${id}`, window.location.href);
        url.protocol = url.protocol.replace('http', 'ws');

        const socket = new WebSocket(url.href)
        socket.onclose = () => {
            if(this.state.closeIsError) {
                this.setState({error: 'WebSocket closed prematurely'})
            }
        }
        socket.onmessage = this.onMessage.bind(this)
        socket.onerror = (err) => this.setState({error: `WebSocket error: ${err.type}`})
        this.setState({
            closeIsError: true,
            socket: socket
        })
    }

    requestDownload() {
        axios.post(`/api/core/download`, {})
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then((data) => {
                this.setState({
                    id: data.id
                })
                this.connectSocket(data.id)
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
    }

    componentDidMount() {
        this.requestDownload()
    }

    componentWillUnmount() {
        console.log("Bye bye")
        if (this.state.socket) {
            console.log("Closing socket")
            this.state.socket.close()
        }
    }

    errorMessage() {
        return (
            <Row>
                <Col>
                    <Alert variant="danger">{this.state.error}</Alert>
                </Col>
            </Row>
        )
    }

    progressBar() {
        if (this.state.done) {
            return (<Row className="mb-3">
                <Col>
                    <ProgressBar now="100" label="Ready" variant="success" style={{height: '30px'}}/>
                </Col>
            </Row>)
        } else {
            return (<Row className="mb-3">
                <Col>
                    <ProgressBar striped animated now={this.state.progress} label={this.state.status} style={{height: '30px'}}  />
                </Col>
            </Row>)
        }
    }

    cancel() {
        const encodedId = encodeURIComponent(this.state.id)
        axios.delete(`/api/core/download/${encodedId}`)
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then(() => {
                this.props.history.push("/")
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
    }

    download() {
        const encodedId = encodeURIComponent(this.state.id)
        axios.get(`/api/core/download/${encodedId}`, {responseType: 'blob'})
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res
            })
            .then((res) => {
                const date = new Date().toISOString()
                const filename = `mister-${date}.zip`
                fileDownload(res.data, filename)
                this.cancel()
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
    }

    content() {
        return (
            <React.Fragment>
                <Row className="mb-3">
                    <Col>
                        Your MiSTer SD card archive is being prepared. Once the download is complete
                        you will be able to use the Download button to download the file.
                    </Col>
                </Row>
                {this.progressBar()}
                <Row>
                    <Col>
                        <Button variant="success" disabled={!this.state.done} onClick={this.download.bind(this)}>
                            <AiOutlineCloudDownload/> Download
                        </Button>
                        &nbsp;
                        <Button variant="danger" disabled={!this.state.done} onClick={this.cancel.bind(this)}>
                            <ImCancelCircle/> Cancel
                        </Button>
                    </Col>
                </Row>
            </React.Fragment>
        )
    }

    render() {
        return (
            <Container>
                {this.state.error !== null && this.errorMessage()}
                {this.state.error === null && this.content()}
            </Container>
        )
    }
}

Download.contextType = AuthContext

export default withRouter(Download)
