import React, {Component} from 'react'
import {Alert, Button, Col, Container, Row} from "react-bootstrap";
import Core from "./Core";
import {LinkContainer} from 'react-router-bootstrap'
import {AuthContext} from "../auth/AuthContext";
import axios from "axios";
import {AiOutlineCloudDownload, BiRefresh, RiAddFill} from "react-icons/all";

class Cores extends Component {

    state = {
        cores: [],
        error: null,
        versions: {},
        loading: false
    }

    refresh() {
        this.setState({
            loading: true
        })
        axios.get('/api/core')
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then((data) => {
                this.setState({ cores: data })
            })
            .catch(error => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            }).finally(() => {
                this.setState({
                    loading: false
                })
        })
    }

    cacheRefresh() {
        this.setState({
            cores: [],
            loading: true
        })
        axios.post('/api/core/refresh', {})
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .catch(error => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            }).finally(() => {
                this.refresh()
        })
    }

    componentDidMount() {
        this.refresh()
    }

    addOne() {
        this.setState((state) => {
                const newCores = [{name: "new", "repo": "new/one" }]
                return ({
                    cores: [...state.cores, ...newCores]
                })
            }
        )
    }

    send() {
        console.log(this.state.versions)
    }

    render() {
        let cores = this.state.cores.map((it) =>
            <Core key={it.repo} name={it.name} repo={it.repo} version={it.version || ""}
                  onDelete={this.refresh.bind(this)}
            />
        );

        return (
            <Container>
                {this.state.error &&
                    <Row className="mb-3">
                        <Col>
                            <Alert variant="danger">{this.state.error}</Alert>
                        </Col>
                    </Row>
                }
                <Row className="mb-3">
                    <Col>
                        <LinkContainer to="/cores/add">
                            <Button disabled={this.state.loading}>
                                <RiAddFill/> Add
                            </Button>
                        </LinkContainer>
                        &nbsp;
                        <LinkContainer to="/cores/download">
                            <Button disabled={this.state.loading}>
                                <AiOutlineCloudDownload/> Download
                            </Button>
                        </LinkContainer>
                        &nbsp;
                        <Button onClick={this.cacheRefresh.bind(this)} disabled={this.state.loading}>
                            <BiRefresh/> Refresh
                        </Button>
                    </Col>
                </Row>

                {this.state.loading &&
                    <Row>
                        <Col>
                            Loading...
                        </Col>
                    </Row>
                }

                {this.state.loading === false && <Row xs={1} md={2} xxl={3}>
                    {cores}
                </Row>}
            </Container>
        )
    }
}

Cores.contextType = AuthContext

export default Cores
