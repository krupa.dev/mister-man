import React, {Component} from 'react'
import {Alert, Button, Col, Container, Form, InputGroup, Row} from "react-bootstrap";
import {AuthContext} from "../auth/AuthContext";
import {withRouter} from "react-router-dom";
import {Typeahead} from "react-bootstrap-typeahead";
import {AiFillDelete} from "react-icons/all";
import axios from "axios";

class AddCore extends Component {

    state = {
        cores: [],
        name: "",
        repo: "",
        loading: false,
        error: null,
        ref: React.createRef()
    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        axios.get('/api/core/available')
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then((data) => {
                this.setState({
                    cores: data
                })
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
            .finally(() => {
                this.setState({
                    loading: false
                })
            })
    }

    addCore() {
        this.setState({
            loading: true
        })
        axios.post('/api/core', {repo: this.state.repo, name: this.state.name})
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then((data) => {
                this.props.history.push("/")
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
            .finally(() => {
                this.setState({
                    loading: false
                })
            })
    }

    updateRepo(item) {
        this.setState({
            repo: item[0]
        })
    }

    updateName(event) {
        this.setState({
            name: event.target.value
        })
    }

    clearSelection() {
        this.state.ref.current.clear()
        this.setState({
            repo: ""
        })
    }

    isValid() {
        return this.state.cores.includes(this.state.repo)
    }

    validityClass() {
        if (this.isValid()) {
            return "is-valid"
        } else {
            return "is-invalid"
        }
    }

    formContent() {
        return (
            <Form>
                <Form.Group controlId="select-core" className="mb-3">
                    <Form.Label>RepositoryX</Form.Label>
                    <InputGroup>
                        <Typeahead className={this.validityClass()}
                                   id="select-core"
                                   isValid={this.isValid()}
                                   isInvalid={!this.isValid()}
                                   ref={this.state.ref}
                                   options={[""].concat(this.state.cores)}
                                   onChange={this.updateRepo.bind(this)} />
                        <Button onClick={this.clearSelection.bind(this)} variant="danger">
                            <AiFillDelete/>
                        </Button>
                        <Form.Control.Feedback type="invalid">Please select a core</Form.Control.Feedback>
                        <Form.Control.Feedback type="valid">This core will be added</Form.Control.Feedback>
                    </InputGroup>
                </Form.Group>
                <Form.Group controlId="add-name" className="mb-3">
                    <Form.Label>Name</Form.Label>
                    <Form.Control disabled={this.state.loading} type="text"  onChange={ this.updateName.bind(this) } placeholder="This is a friendly name for the repository"/>
                    <Form.Text className="text-muted">
                        This is a friendly name for the repository
                    </Form.Text>
                </Form.Group>
                <Form.Group className="mb-3">
                    <Button disabled={this.state.loading || !this.isValid()} onClick={this.addCore.bind(this)}>Add</Button>
                </Form.Group>
            </Form>
        )
    }

    errorMessage() {
        return (
            <Alert variant="danger">{this.state.error}</Alert>
        )
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col>
                        {this.state.error !== null && this.errorMessage()}
                        {this.formContent()}
                    </Col>
                </Row>
            </Container>
        )
    }
}

AddCore.contextType = AuthContext

export default withRouter(AddCore)
