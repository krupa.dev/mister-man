import React, {Component} from 'react'
import {Button, Card, CloseButton, Col, Form, InputGroup} from "react-bootstrap";
import {AuthContext} from "../auth/AuthContext";
import {AiOutlineLaptop, BsClock, CgPacman} from "react-icons/all";
import axios from "axios";

class Core extends Component {
    state = {
        versions: [],
        selectedVersion: "",
        loading: false,
        deleting: false,
        saving: false,
        error: null,
    }

    componentDidMount() {
        this.setState({
            loading: true,
            selectedVersion: this.props.version
        })
        const encodedRepo = encodeURIComponent(this.props.repo)
        axios.get(`/api/core/${encodedRepo}`)
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then((data) => {
                this.setState({
                    versions: data
                })
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
            .finally(() => {
                this.setState({
                    loading: false
                })
            })
    }

    updateSelectedVersion(data) {
        this.setState({
            selectedVersion: data.target.value
        })
        this.setState({
            saving: true
        })
        const encodedRepo = encodeURIComponent(this.props.repo)
        axios.put(`/api/core/${encodedRepo}`, {version: data.target.value})
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then(() => {
                // Nada
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
            .finally(() => {
                this.setState({
                    saving: false
                })
            })
    }

    delete() {
        this.setState({
            deleting: true
        })
        const encodedRepo = encodeURIComponent(this.props.repo)
        axios.delete(`/api/core/${encodedRepo}`)
            .then(res => {
                if (res.status < 200 || res.status > 299) {
                    throw new Error(`Got error code ${res.status}`)
                } else return res.data
            })
            .then(() => {
                this.props.onDelete(this.props.repo)
            })
            .catch((error) => {
                console.log(error)
                this.setState({
                    error: error.message
                })
            })
    }

    selectLatest() {
        if (this.state.selectedVersion !== this.state.versions[0]) {
            this.updateSelectedVersion({
                target: {
                    value: this.state.versions[0]
                }
            })
        }
    }

    isDisabled() {
        return this.state.saving || this.state.deleting || this.state.loading
    }

    unselected() {
        return this.state.selectedVersion === '' || this.state.selectedVersion === null
    }

    latestSelected() {
        return this.state.versions.length > 0 && this.state.selectedVersion === this.state.versions[0]
    }

    background() {
        if (this.state.selectedVersion === '') {
            return 'danger'
        } else if (this.latestSelected()) {
            return null
        } else {
            return 'warning'
        }
    }

    render() {
        const versions = this.state.versions.map((it) =>
            <option key={it} value={it}>{it}</option>
        )

        return (
            <Col>
                <Card className="mb-3" bg={this.background()} border="dark">
                    <Card.Header>
                        <CloseButton className="float-end"
                                onClick={this.delete.bind(this)}
                                disabled={this.isDisabled()}/>
                        <Card.Title>
                            {!this.props.repo.includes("Arcade") && <AiOutlineLaptop/>}
                            {this.props.repo.includes("Arcade") && <CgPacman/>}
                            &nbsp;
                            {this.props.name || this.props.repo}
                        </Card.Title>
                        {this.props.name && <Card.Subtitle>{this.props.repo}</Card.Subtitle>}
                        {this.state.error}
                    </Card.Header>
                    <Card.Body>
                        <Form.Group controlId="bob">
                            <Form.Label>Selected Version</Form.Label>
                            {this.state.versions.length === 0 && <div>Loading...</div>}
                            {this.state.versions.length > 0 &&
                            <InputGroup>
                                <Form.Select aria-label="Name"
                                             className={this.state.selectedVersion === '' && "is-invalid"}
                                             value={this.state.selectedVersion}
                                             onChange={ this.updateSelectedVersion.bind(this) }
                                             disabled={this.isDisabled()}>
                                    {this.unselected() && <option value="">Please Select...</option>}
                                    {versions}
                                </Form.Select>
                                <Button onClick={this.selectLatest.bind(this)}
                                        disabled={this.isDisabled() || this.latestSelected()}>
                                    <BsClock/>&nbsp;Latest
                                </Button>
                            </InputGroup>
                            }

                        </Form.Group>
                    </Card.Body>
                    <Card.Footer>
                        {this.state.selectedVersion === '' && 'Please select a version'}
                        {this.latestSelected() && 'The latest version is selected'}
                        {this.state.selectedVersion !== '' && !this.latestSelected() && 'A more up-to-date version is available'}
                    </Card.Footer>
                </Card>
            </Col>
        )
    }
}

Core.contextType = AuthContext

export default Core
