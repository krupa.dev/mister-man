# mister-man

WebApp for downloading and monitoring cores for the MiSTer FGPA emulation board

## Building
 
To build this simply run a Maven build/package
To create a local ARM64 Docker image for testing run ./local-build.sh
To create a local AMD64 Docker image for testing run docker build -f src/main/docker/Dockerfile.jvm .

## Running

To run locally with a database you can use the docker-compose.yaml file in the test_db directory. Database
data will be persisted into the pg_data subdirectory.

To run this you will need a valid GitHub application client ID and client secret.

The app can take a number of environment variables for configuration. Any of these can be omitted if the default
value is required:

| Name          | Meaning                                                | Default      |
|---------------|--------------------------------------------------------|--------------|
| DB_HOST       | Postgresql database hostname                           | localhost    |
| DB_PORT       | Postgresql database port                               | 5432         |
| DB_USERNAME   | Database user username                                 | postgres     |
| DB_PASSWORD   | Database user password                                 | password     |
| CLIENT_ID     | GitHub app client ID                                   |              |
| CLIENT_SECRET | GitHub app client ID                                   |              |
| REFRESH_CRON  | Schedule for refreshing the list of cores and versions | 0 0 20 * * ? |
