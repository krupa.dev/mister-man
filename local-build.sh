#!/usr/bin/env bash
docker buildx create --use
docker buildx build \
  -f src/main/docker/Dockerfile.jvm \
  --platform=linux/arm64 \
  -t "mister-man-local:latest" \
  --progress=plain \
  --load \
  --cache-from=type=inline \
  --cache-to=type=inline \
  .
